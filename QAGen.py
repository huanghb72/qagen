""" Automated generation of QA pairs from text document
"""
import sys,getopt

# Command line inputs
def main(argv):
    input_file = 'test.txt'
    output_file = 'output.txt'
    try:
        opts, args = getopt.getopt(argv,"i:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print ('python QAGen.py -i <input text file> -o <output text file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg
    return input_file,output_file


''' Main processing module '''
if __name__ == "__main__":
    input_file,output_file = main(sys.argv[1:])
    print('Input file:',input_file)

    print('processing...')

    print('Output saved:',output_file)
